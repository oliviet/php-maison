<?php
session_start();
require 'header.php';
require 'connect.php';

$client="";	
$context="";
$status="";
$objective="";	
$presentation="";
$output="";
$db = connect();

    if (!empty($_POST)){

        $client = checkInput($_POST['client']);
        $context = checkInput($_POST['context']);
        $status = checkInput($_POST['status']);
        $objective = checkInput($_POST['objective']);
        $presentation = checkInput($_POST['presentation']);
        $output = checkInput($_POST['output']);
        $formsuccess = true;
    }

    if (empty($client)){
        $formsuccess=false;
    }

    if (empty($context)){
        $formsuccess=false;
    }
    if (empty($status)){
        $formsuccess=false;
    }
    if (empty($objective)){
        $formsuccess=false;
    }
    if (empty($presentation)){
        $formsuccess=false;
    }
    if (empty($output)){
        $formsuccess=false;
    }

if($formsuccess){
    $db = connect();
    $up = $db->prepare("INSERT INTO `reference`(`client`, `context`, `status`, `objective`, `presentation`, `output`) VALUES (?,?,?,?,?,?)");
    $up->execute(array($client,$context,$status,$objective,$presentation,$output));
    header("location: references.php");
    //echo 'Nice !';
}
    

    $pdo = $db->prepare("SELECT `id`, `client`, `context`, `status`, `objective`, `presentation`, `output` FROM `reference`");
    $pdo ->execute();
    $result = $pdo->fetch(PDO::FETCH_ASSOC);

        function checkInput($data)
        {
            $data = trim($data);
            $data = stripslashes($data);
            $data = htmlspecialchars($data);
            return $data;
        }


?>

        <main role="main"> <!-- main -->

        <table class="table table-responsive"> <!-- table -->
            <thead>
            <tr>
                <th scope="col">ID</th>
                <th scope="col">Client</th>
                <th scope="col">Context</th>
                <th scope="col">Status</th>
                <th scope="col">Objective</th>
                <th scope="col">Presentation</th>
                <th scope="col">Output</th>
                <!--<th scope="col">CreatedAT</th>-->
            </tr>
            </thead>

            <tbody>
                <?php   
                    if($result){
                        while($result = $pdo->fetch(PDO::FETCH_ASSOC)){
                            echo "<tr><td>" . $result['id'] . "</td>";
                            echo "<td>" . $result['client'] . "</td>";
                            echo "<td>" . $result['context'] . "</td>";
                            echo "<td>" . $result['status'] . "</td>";
                            echo "<td>" . $result['objective'] . "</td>";
                            echo "<td>" . $result['presentation'] . "</td>";
                            echo "<td>" . $result['output'] . "</td>";
                            echo "<td>" . '<a href="view.php?id='.$result['id'].'" class="btn btn-primary">View</a>
                            <a href="edit.php?id='.$result['id'].'"class="btn btn-success">Edit</a>
                            <a href="delete.php?id='.$result['id'].'"class="btn btn-danger">Delete</a>
                            ' . "</td></tr>";
                            
                        }
                    }
                ?>
            </tbody>
        </table> <!-- /table -->

        <div class="row">
            <div class="col-lg-12">
                <form role='form' class='form' enctype='multipart/form-data' method="POST">

                    <h4>Client</h4>
                    <input type="text" class='form-control' id=client name='client' placeholder="Client" required >

                    <h4>Status</h4>
                    <select class='form-control' name='status'>
                        <option value="publish">Publish</option>
                        <option value="unpublished">Unpublished</option>
                        <option value="draft">Draft</option>
                    </select>
            </div>

            <div class="col-lg-6">
                <h4>Context :</h4>
                <textarea type="text" class='form-control' id='context' name='context' required ></textarea>

                <h4>Project presentation :</h4>
                <textarea type="text" class='form-control' id='presentation' name='presentation' required></textarea>
            </div>

            <div class="col-lg-6">
                <h4>Objective :</h4>
                <textarea type="text" class='form-control' id='objective' name='objective' required></textarea>

                <h4>Output</h4>
                <textarea type="text" class='form-control' id='output' name='output' required></textarea>
            </div>

            <div class="col-lg-12">
                <input type="submit" class="btn btn-success" value="Add reference">
            </div>
            </form>
        </div>
    </main>

   <?php

   require 'footer.php';
   ?>

</div> <!-- /container -->
</body>
</html>