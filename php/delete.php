<?php
    require'header.php';
    require'connect.php';

    if(!empty($_GET['id'])){

        $id = checkInput(($_GET['id']));

    }

    if(!empty($_POST['id'])){

        $id = checkInput(($_POST['id']));
        $db = connect();
        $delete = $db->prepare('DELETE FROM `reference` WHERE id = ?');
        $delete->execute(array($id));
        header('location:references.php');

    }



    function checkInput($data)
        {
            $data = trim($data);
            $data = stripslashes($data);
            $data = htmlspecialchars($data);
            return $data;
        }
?>
<link href="css/delete.css" rel="stylesheet" type="text/css">
<h3>Voulez-vous vraiment supprimer ce post ?</h3>

<form action="delete.php" method="post" role="form">
    <input type="hidden" name="id" value="<?php echo $id; ?>">
        <div class="button">
                <button type="submit">Oui</button>
                ou
                <a href="references.php">Non</a>
        </div>
</form>

