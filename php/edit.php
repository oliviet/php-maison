<?php
require 'header.php';
require 'connect.php';

$client="";	
$context="";
$status="";
$objective="";	
$presentation="";
$output="";
$db = connect();
$formsuccess = "";


if(!empty($_GET['id'])) 
{
    $id = checkInput($_GET['id']);
}

if (!empty($_POST)){

    $client = checkInput($_POST['client']);
    $context = checkInput($_POST['context']);
    $status = checkInput($_POST['status']);
    $objective = checkInput($_POST['objective']);
    $presentation = checkInput($_POST['presentation']);
    $output = checkInput($_POST['output']);
    $formsuccess = true;

if (empty($client))
{
    $formsuccess=false;
}

if (empty($context))
{
    $formsuccess=false;
}
if (empty($status))
{
    $formsuccess=false;
}
if (empty($objective))
{
    $formsuccess=false;
}
if (empty($presentation))
{
    $formsuccess=false;
}
if (empty($output))
{
    $formsuccess=false;
}

}

if($formsuccess)
{
    $db = connect();
    $pdo = $db->prepare("UPDATE reference  set client = ?, context = ?, status = ?, objective = ?, presentation = ?, output = ? WHERE id = ?");
    $pdo ->execute(array($client, $context, $status, $objective, $presentation, $output, $id));
    header("location: references.php");

}
else
{
    $db = connect();
    $pdo = $db->prepare("SELECT * FROM reference where id = ?");
    $pdo ->execute(array($id));
    $result = $pdo->fetch();
    $client= $result['client'];	
    $context= $result['context'];
    $status = $result['status'];
    $objective= $result['objective'];	
    $presentation =$result['presentation'];
    $output= $result['output'];
}



function checkInput($data)
        {
            $data = trim($data);
            $data = stripslashes($data);
            $data = htmlspecialchars($data);
            return $data;
        }
?>

<div class="row">
            <div class="col-lg-12">
                <form role='form' class='form' action="<?php echo 'edit.php?id='.$id;?>" enctype='multipart/form-data' method="POST">

                    <h4>Client</h4>
                    <input type="text" class='form-control' id=client name='client' placeholder="Client" required value='<?php echo $client;?>'>

                    <h4>Status</h4>
                    <select class='form-control' name='status' value='<?php echo $status;?>'>
                        <option value="publish">Publish</option>
                        <option value="unpublished">Unpublished</option>
                        <option value="draft">Draft</option>
                    </select>
            </div>

            <div class="col-lg-6">
                <h4>Context :</h4>
                <textarea type="text" class='form-control' id='context' name='context' required><?php echo $context;?></textarea>

                <h4>Project presentation :</h4>
                <textarea type="text" class='form-control' id='presentation' name='presentation' required ><?php echo $presentation;?></textarea>
            </div>

            <div class="col-lg-6">
                <h4>Objective :</h4>
                <textarea type="text" class='form-control' id='objective' name='objective' required><?php echo $objective;?></textarea>

                <h4>Output</h4>
                <textarea type="text" class='form-control' id='output' name='output' required><?php echo $output;?></textarea>
            </div>

            <div class="col-lg-12">
                <input type="submit" class="btn btn-success" value="Add reference">
            </div>
            </form>
        </div>

